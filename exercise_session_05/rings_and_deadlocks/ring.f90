!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! int MPI_Recv(void *buf, int count, MPI_Datatype datatype, int source,
!              int tag, MPI_Comm comm, MPI_Status *status);

! int MPI_Ssend(const void *buf, int count, MPI_Datatype datatype, int dest,
!               int tag, MPI_Comm comm);

program ring

  use mpi

  implicit none

  ! Variables declaration
  integer :: ierror
  integer :: my_rank, left_rank, right_rank, n_proc
  integer :: tmp, my_sum, i

  ! Begin MPI
  call MPI_INIT(ierror)

  ! Get my_rank and n_proc
  call MPI_COMM_RANK(MPI_COMM_WORLD, my_rank, ierror)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, n_proc, ierror)

  left_rank = mod(my_rank-1+n_proc, n_proc)
  right_rank = mod(my_rank+1, n_proc)
  my_sum = 0

  ! The idea of the exercise is that each rank will
  ! - first send their own rank to the right,
  ! - receive their left neighbour rank,
  ! - add what they received into my_sum,
  ! - then send what they received to the right.

  tmp = my_rank
  if(mod(my_rank, 2) .eq. 0) then
    do i=1,n_proc
      ! - first send their own rank to the right,
      call MPI_SSend(tmp, 1, MPI_INT, right_rank, &
                     1, MPI_COMM_WORLD, ierror)
      print *, "I am ", my_rank, " and send ", tmp, "to ", right_rank
      ! - receive their left neighbour rank,
      call MPI_Recv(tmp, 1, MPI_INT, left_rank, 1, &
                    MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierror)
      print *, "I am ", my_rank, " and received ", tmp, "from ", left_rank
      ! - add what they received into my_sum,
      my_sum = my_sum + tmp
    enddo
  else
    do i=1,n_proc
      call MPI_Recv(tmp, 1, MPI_INT, left_rank, 1, &
                    MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierror)
      print *, "I am ", my_rank, " and received ", tmp, "from ", left_rank
      my_sum = my_sum + tmp
      call MPI_SSend(tmp, 1, MPI_INT, right_rank, &
                     1, MPI_COMM_WORLD, ierror)
      print *, "I am ", my_rank, " and send ", tmp, "to ", right_rank
    enddo
  endif

  write(*,*) 'This is ',my_rank,' out of ',n_proc,'and the sum is',my_sum

  call MPI_FINALIZE (ierror)

end program ring

