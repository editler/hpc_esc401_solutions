#!/bin/bash -l
#SBATCH --job-name="name"
#SBATCH --time=00:01:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=12
#SBATCH --cpus-per-task=1
#SBATCH --partition=debug
#SBATCH --constraint=gpu
#SBATCH --hint=nomultithread
#SBATCH --account=uzg2

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
module load daint-gpu
srun $1 
