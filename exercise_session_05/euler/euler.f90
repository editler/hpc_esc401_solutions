!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! int MPI_Recv(void *buf, int count, MPI_Datatype datatype, int source,
!              int tag, MPI_Comm comm, MPI_Status *status);

! int MPI_Ssend(const void *buf, int count, MPI_Datatype datatype, int dest,
!               int tag, MPI_Comm comm);

! call MPI_SSend(tmp, 1, MPI_INT, right_rank, &
!                1, MPI_COMM_WORLD, ierror)
! print *, "I am ", my_rank, " and send ", tmp, "to ", right_rank
! call MPI_Recv(tmp, 1, MPI_INT, left_rank, 1, &
!               MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierror)
! print *, "I am ", my_rank, " and received ", tmp, "from ", left_rank

    program ring

    use mpi

    implicit none
    integer, parameter :: dp = selected_real_kind(14, 200)

    ! Variables declaration
    integer(8) :: iproc, n
    integer :: ierror
    integer :: my_rank, n_proc, from, to
    real(dp) :: my_e, last_fac, tmp

    n = 1000000000

    ! Begin MPI
    call MPI_INIT(ierror)

    ! Get my_rank and n_proc
    call MPI_COMM_RANK(MPI_COMM_WORLD, my_rank, ierror)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, n_proc, ierror)

    if (my_rank .eq. 0) then
        ! master process
        ! tell each process what to do
        print *, 'n: ', n
        do iproc=1,n_proc-1
            call MPI_Recv(tmp, 1, MPI_DOUBLE, iproc, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierror)
            print *, 'I am', my_rank, 'and received', tmp, 'from', iproc
            my_e = my_e + tmp
        enddo
        print *, 'I am', my_rank, 'and have', my_e
    else
        from = n*(my_rank-1)/(n_proc-1)
        to = n*(my_rank)/(n_proc-1)
        print *, 'I am', my_rank, 'and go',from,'to',to

        do n=from,to
            if (n .lt. 2) then
                my_e = 1._dp
                last_fac = 1._dp
                continue
            endif

            last_fac = last_fac/real(n, dp)
            my_e = my_e + last_fac
        enddo

        print *, 'I am', my_rank, 'and have', my_e
        call MPI_SSend(my_e, 1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, ierror)
    endif

    call MPI_FINALIZE (ierror)

end program ring


