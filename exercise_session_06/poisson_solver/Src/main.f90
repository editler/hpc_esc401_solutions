!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -*- Mode: F90 -*- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! main.f90 --- 
!!!!
!! program poisson_main
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program poisson_main
    use poisson_commons
    use poisson_parameters
    use poisson_IO
    use poisson_principal
    use poisson_mpi

    implicit none
    real(kind=prec_real) :: start, finish

    ! Initialize MPI environment
    call MPI_INIT(ierror)

    ! read parameters from input file
    call read_params
    ! initialize MPI domains
    call init_mpi

    call MPI_BARRIER(MPI_COMM_WORLD, ierror)
    if (myrank == 0) then
        start = MPI_WTIME()
        print *, 'start', start
    endif
    ! initialize variables according to inputs
    call init_poisson
    ! output the exact solution
    call output_exact


    ! loop until error tolerance is satisfied
    do
        ! print *, myrank, 'start do', nstep
        ! output approximate solution each noutput iterations
        if(MOD(nstep,noutput)==0) then
            if (myrank == 0) then 
                print*,'New step, nstep = ',nstep,', diff = ',diff,', error = ',error
            end if
            call output
        end if

        ! use jacobi solver
        call jacobi_step
        nstep = nstep + 1

        ! check tolerance
        if ( diff <= tolerance ) then
            converged = .true.
            exit
        end if 

    end do

    
    call MPI_BARRIER(MPI_COMM_WORLD, ierror)
    if (myrank == 0) then
        print*, 'Done'
        finish = MPI_WTIME()
        print *, 'f', finish, 's', start
        print *, 'Took', finish-start
    endif
    call MPI_FINALIZE(ierror)


end program poisson_main