program minimal
use mpi
implicit none

integer, parameter :: prec_real=kind(1.d0)

real(kind=prec_real), dimension(4) :: the_field
real(kind=prec_real), dimension(2) :: send_buffer, receive_buffer

integer :: ierror
integer :: nproc, myrank, send_to, receive_from
integer, dimension(2) :: requests
integer, dimension(MPI_STATUS_SIZE, size(requests)) :: status

the_field = 0._prec_real

call MPI_INIT(ierror)
call MPI_COMM_RANK(MPI_COMM_WORLD, myrank, ierror)
call MPI_COMM_SIZE(MPI_COMM_WORLD, nproc, ierror)

if (myrank==0) then
    print *,' MPI Execution with ',nproc,' processes'
    send_to = 1
    receive_from = 1
    send_buffer = 1000
else
    send_to = 0
    receive_from = 0
    send_buffer = -1000
endif

if (myrank == 0) then
    call MPI_IRECV(receive_buffer, 2, MPI_DOUBLE, &
                   receive_from, 1, &
                   MPI_COMM_WORLD, requests(1), ierror)

    call MPI_ISEND(send_buffer, 2, MPI_DOUBLE, &
                   receive_from, 1, &
                   MPI_COMM_WORLD, requests(2), ierror)
endif


if (myrank == 1) then
    call MPI_IRECV(receive_buffer, 2, MPI_DOUBLE, &
                   receive_from, 1, &
                   MPI_COMM_WORLD, requests(1), ierror)

    call MPI_ISEND(send_buffer, 2, MPI_DOUBLE, &
                   receive_from, 1, &
                   MPI_COMM_WORLD, requests(2), ierror)
endif

call MPI_WAITALL(2, requests, status, ierror)
print *, myrank, 'I received', receive_buffer

if (myrank==0) then
    the_field(1:2) = receive_buffer
else
    the_field(3:4) = receive_buffer
endif
print *, myrank, 'the_field', the_field




call MPI_FINALIZE(ierror)

end program minimal