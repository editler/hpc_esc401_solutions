#!/usr/bin/env bash

for i in {0..3}; do
  echo "Running sum_$i";
  cc -O$i -o sum_$i sum.c gettime.c;
  time ./sum_$i
done

echo "Running sum_3_plus";
cc -ffast-math -mavx2 -O3 -o sum_3_plus sum.c gettime.c
./sum_3_plus
