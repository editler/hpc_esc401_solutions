#!/usr/bin/env bash

# Give a regular expression that will match only the binary strings that satisfy the condition

# end with 00
grep "00$" binary.txt

# start and end with 1
grep "^1.*1$" binary.txt
# such a line does not exist in the given binary.txt

# contain the pattern 110
grep "110" binary.txt

# contain at least three times a 1
grep -E "1{3,}" binary.txt

# contain at least three consecutive 1s
grep -E "*(1){3,}*" binary.txt
