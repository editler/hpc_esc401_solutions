#!/usr/bin/env bash
if [[ $# -ne 1 ]]; then
  echo "USAGE: $0 filename";
  exit 1;
fi

if test -e $1; then
  while read l; do
    printf "$l\n\n";
  done < $1 
else
  echo "File $1 not found!";
  exit 2;
fi

# now it ran correctly, so
while read l; do
  printf "$l\n\n\n";
done < $1 > ${1}.tmp
mv ${1}.tmp ${1}

#  2020-03-14 15:03:25 ⌚  daint106 in ~/hpc_esc401_solutions/exercise_session_04
#± |master ↑2 ?:6 ✗| → ./exercise3.bash exercise3.text1
#File exercise3.text1 not found!
#
# 2020-03-14 15:03:26 ⌚  daint106 in ~/hpc_esc401_solutions/exercise_session_04
#± |master ↑2 ?:6 ✗| → ./exercise3.bash exercise3.text
#One line
#
#Two line
#
#Three line
#
#
#
#Blank line?
#
#
# 2020-03-14 15:03:28 ⌚  daint106 in ~/hpc_esc401_solutions/exercise_session_04
#± |master ↑2 ?:6 ✗| → ./exercise3.bash
#USAGE: ./exercise3.bash filename
