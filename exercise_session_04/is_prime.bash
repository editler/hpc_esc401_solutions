#!/usr/bin/env bash

# Get a number
if [[ $# -eq 1 ]]; then
  NUMBER=$1;
else
  read NUMBER;
fi

#echo "Input: $NUMBER";

# is it really a number?
  # the regex must not be quoted...
if ! [[ $NUMBER =~ ^[0-9]+$ ]]; then
  echo "Not a number!"
  echo $NUMBER;
  exit 1;
fi

# We only need to check until sqrt(N), but just to be sure... 
UPPER_LIMIT=$(echo "scale=0; sqrt($NUMBER+1)" | bc -l)

for i in $(seq 2 $UPPER_LIMIT); do
  # integer division - kind of
  int_div=$(echo "scale=0;${NUMBER}%$i" | bc -l);
  if [[ $int_div -eq 0 ]]; then
    echo "$1 is not prime!";
    exit 0;
  fi
done

echo "$NUMBER is prime!";
